import React from 'react';
import '../Button/button.css';

const Button = (props) => {
	return (
		<>
			<button
				type='button'
				className={props.styleclass}
				onClick={props.onClick}
			>
				{props.text}
			</button>
		</>
	);
};

export default Button;
