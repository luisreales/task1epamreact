import React from 'react';
import '../Input/input.css';
const Input = ({ id, name, estilo, text, onChange, value, style, type }) => {
	const Label = () => {
		return name === undefined || name === '' ? '' : <label>{name}:</label>;
	};
	return (
		<>
			<Label />
			<input
				id={id}
				type={type}
				className={estilo}
				placeholder={text}
				onChange={onChange}
				value={value}
				style={style}
				name={name}
			/>
		</>
	);
};

export default Input;
