import React from 'react';
import './App.css';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';

function App() {
	return (
		<>
			<div className='container-fluid bg-dark alinear'>
				<Header />
			</div>
			<div className='container'>
				<Courses />
			</div>
		</>
	);
}

export default App;
