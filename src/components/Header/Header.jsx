import React from 'react';
import Logo from './components/Logo/Logo.jsx';
import Button from '../../common/Button/Button';
import '../Header/header.css';

const Header = () => {
	const username = 'Dave';
	const stylebtn = 'btn btn-primary';
	return (
		<>
			<div className='container'>
				<div className='row'>
					<div className='col-md-6'>
						<Logo />
					</div>
					<div className='col-md-6'>
						<div className='button-division'>
							<span>{username}</span>{' '}
							<Button text={'Logout'} styleclass={stylebtn}>
								Logout
							</Button>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default Header;
