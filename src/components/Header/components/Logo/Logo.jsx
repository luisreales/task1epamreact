import React from 'react';
import LogoCourse from '../../../../assets/courses.png';
import '../Logo/logo.css';

const Logo = () => {
	return (
		<>
			<img className='logo' src={LogoCourse} alt='Courses' />
			<span className='spantexto'>iCoding Academy Courses</span>
		</>
	);
};

export default Logo;
