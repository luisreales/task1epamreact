import React from 'react';
import Button from '../../../../common/Button/Button';
import '../CourseCard/coursecard.css';

const CourseCard = (props) => {
	const { title, description, creationDate, duration } = props.course;
	const authorslist = props.authorslist;

	// const Authors = () => {
	// 	return authorslist.map((author, index) => {
	// 		return <li key={index}>{author}</li>;
	// 	});
	// };

	console.log(authorslist);
	return (
		<>
			<div className='coursecard'>
				<div className='row'>
					<div className='col-8'>
						<h2>{title}</h2>
						<p>{description}</p>
					</div>
					<div className='col-4'>
						<div>
							<label>Authors: {authorslist}</label>
						</div>
						<div>
							<label>Duration: {duration}</label>
						</div>
						<div>
							<label>Created:{creationDate} </label>
						</div>
						<div>
							<Button text={'Show Course'} />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default CourseCard;
