import React, { useState } from 'react';
import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';
import '../SearchBar/searchbar.css';
const SearchBar = ({
	course,
	setFilterCourse,
	setCreateCourse,
	setDataCourse,
}) => {
	const estilo = 'expand';
	const stylebtn = 'btn btn-primary btn-sm';
	const styleblack = 'btn btn-dark btn-sm';

	const [filterInput, setFilterInput] = useState('');

	const createCourseChange = (value) => {
		setCreateCourse(value);
	};
	const filtrarChange = (e) => {
		if (e === '') {
			setFilterCourse('');
		} else {
			setFilterInput(e);
		}
	};
	const filtrarCurso = (filterInput) => {
		setFilterCourse(filterInput);
	};

	return (
		<>
			<div className='row searchbar'>
				<div className='col-md-4'>
					<Input
						text={'Enter course name or id...'}
						estilo={estilo}
						onChange={(e) => filtrarChange(e.currentTarget.value)}
						name={''}
						type={'text'}
					/>
				</div>
				<div className='col-md-4'>
					<Button
						text={'Search'}
						styleclass={stylebtn}
						onClick={() => filtrarCurso(filterInput)}
					/>
				</div>
				<div className='col-md-4 alinear'>
					<Button
						text={'Add new course'}
						styleclass={styleblack}
						onClick={() => createCourseChange(true)}
					/>
				</div>
			</div>
			<br />
		</>
	);
};

export default SearchBar;
