import React, { useState } from 'react';
import CourseCard from '../../components/Courses/components/CourseCard/CourseCard';
import SearchBar from '../../components/Courses/components/SearchBar/SearchBar';
import CreateCourse from '../../components/CreateCourse/CreateCourse';
import { mockedCoursesList, mockedAuthorsList } from '../../seed.js';

const Courses = () => {
	const [dataCourse, setDataCourse] = useState(mockedCoursesList);
	const [dataAuthors, setDataAuthors] = useState(mockedAuthorsList);
	const [isNewCourse, setCreateCourse] = useState(false);
	const [dataCourseFiltered, setDataCourseFiltered] = useState([]);

	const autoresList = (item) => {
		const authors = item.authors;
		const listauthors = dataAuthors;
		console.log('mockedCoursesList', item, authors);
		console.log('mockedAuthorsList', listauthors);

		const data = authors.map((author) => {
			return authorsfiltado(listauthors, author);
		});
		return data.join(',');
	};

	const authorsfiltado = (listauthors, id) => {
		return listauthors
			.filter((item) => item.id === id)
			.map((result) => result.name);
	};

	const CourseCardItems = () => {
		const courseGridData =
			dataCourseFiltered.length === 0
				? dataCourse.map((item) => (
						<CourseCard
							course={item}
							key={item.id}
							authorslist={autoresList(item)}
						/>
				  ))
				: dataCourseFiltered.map((item) => (
						<CourseCard
							course={item}
							key={item.id}
							authorslist={autoresList(item)}
						/>
				  ));

		return courseGridData;
	};

	const FilterCourse = (title) => {
		if (title === '') {
			setDataCourseFiltered(dataCourse);
		} else {
			const filterData = dataCourse.filter((item) => {
				return item.title.toLowerCase().includes(title.toLowerCase());
			});
			//setDataCourse([...dataCourseFiltered, filterData]);
			setDataCourseFiltered(filterData);
		}
	};

	return (
		<>
			{isNewCourse === true ? (
				<CreateCourse
					dataCourse={dataCourse}
					setCreateCourse={setCreateCourse}
					setDataCourse={setDataCourse}
					dataAuthors={dataAuthors}
					setDataAuthors={setDataAuthors}
				/>
			) : (
				<>
					<SearchBar
						course={mockedCoursesList}
						setFilterCourse={FilterCourse}
						setCreateCourse={setCreateCourse}
						setDataCourse={setDataCourse}
					/>
					<CourseCardItems />
				</>
			)}
		</>
	);
};

export default Courses;
