import React, { useState } from 'react';
import Button from '../../common/Button/Button';
import '../CreateCourse/createcourse.css';
import Input from '../../common/Input/Input';
import pipeDuration from '../../helpers/pipeDuration';
//import { v5 as uuidv5 } from 'uuid';
import * as uuid from 'uuid';

const CreateCourse = ({
	dataCourse,
	setCreateCourse,
	setDataCourse,
	dataAuthors,
	setDataAuthors,
}) => {
	const style = { width: '80%' }; // Object literal notation
	const stylebtn = 'btn btn-primary btn-sm';
	const stylebtnCancel = 'btn btn-danger btn-sm';
	const uuidListaCurso = uuid.v1();
	const uuidListaAuthores = uuid.v1();

	//const [authors, setAuthors] = useState(authorslist);
	//variable para autor name
	const [authorname, setAuthorName] = useState('');
	const [dataAuthorsLocal, setDataAuthorsLocal] = useState(dataAuthors);
	//lista de authores asociados a un curso
	const [courseauthors, setCourseAuthors] = useState([]);

	//variabe para manejar duracion
	const [duration, setDuration] = useState('');
	//objecto para persistir informacion del formulario
	const [dataForm, setDataForm] = useState({
		title: '',
		description: '',
		duration: '',
	});

	const handleInputChange = (e) => {
		setDataForm({
			...dataForm,
			[e.target.name]: e.target.value,
		});
	};

	const saveCourseChange = (value) => {
		debugger;
		const courseauthorsid = courseauthors.map((c) => c.id);

		const courseData = {
			id: uuid.v1(),
			title: dataForm.title,
			description: dataForm.description,
			creationDate: Date.now(),
			duration: dataForm.duration,
			authors: courseauthorsid,
		};

		//save course
		setDataCourse([...dataCourse, courseData]);

		//update list authors
		console.log('curso actualizado', dataCourse);
		setCreateCourse(value);
	};

	const addAuthors = (authorname) => {
		const newAuthor = {
			id: uuid.v1(),
			name: authorname,
		};
		setDataAuthorsLocal((arra) => [...arra, newAuthor]);
		setDataAuthors((arra) => [...arra, newAuthor]);

		setAuthorName('');
	};

	const onChangeAuthorName = (value) => {
		setAuthorName(value);
	};

	const addCourseAuthors = (item) => {
		debugger;
		const courseauthor = {
			id: item.id,
			name: item.name,
		};
		//add new courseauthor to list authors
		setCourseAuthors((course) => [...course, courseauthor]);

		//remove from the list authors

		const authorsfiltered = dataAuthorsLocal.filter((a) => a.id !== item.id);

		// se remueve temporal
		setDataAuthorsLocal(authorsfiltered);
	};
	const ListarAutores = () => {
		// eslint-disable-next-line array-callback-return
		const lista = dataAuthorsLocal.map((item) => {
			return (
				<div className='listautores'>
					<div className='row'>
						<div className='col'>
							<h6>{item.name}</h6>
						</div>
						<div className='col'>
							<Button
								key={item.id}
								text={'Add Author'}
								onClick={() => addCourseAuthors(item)}
							/>
						</div>
					</div>
				</div>
			);
		});

		return lista;
	};

	const removeCourseAuthors = (item) => {
		//remove item from courseauthors list
		const courseahutor = courseauthors.filter((c) => c.id !== item.id);
		setCourseAuthors(courseahutor);

		//add item in authors list
		// const author = {
		// 	id: item.id,
		// 	name: item.name,
		// };

		//se remueve temporal setDataAuthors([...dataAuthors, author]);
	};

	const ListCourseAuthors = () => {
		const lista = courseauthors.map((item) => {
			return (
				<div className='listautores'>
					<div className='row'>
						<div className='col'>
							<h6>{item.name}</h6>
						</div>
						<div className='col'>
							<Button
								key={item.id}
								text={'Delete Author'}
								onClick={() => removeCourseAuthors(item)}
							/>
						</div>
					</div>
				</div>
			);
		});
		return lista;
	};

	const onChangeDuration = (e) => {
		const input = e.target.value;
		if (!Number(input)) {
			return;
		}

		if (input === '') {
			setDuration('');
		} else {
			const durationvalue = pipeDuration(input);
			setDuration(durationvalue);
			setDataForm({
				...dataForm,
				[e.target.name]: e.target.value,
			});
		}
	};

	const cancelCreateCourse = (value) => {
		setCreateCourse(value);
	};
	return (
		<>
			<div className='createcourse'>
				<div className='row'>
					<div className='col-md-6'>
						<Input
							id={'title'}
							text={'Enter a title'}
							onChange={handleInputChange}
							name={'title'}
							type={'text'}
						/>
					</div>
					<div className='col-md-6'>
						<div className='createcourse-right'>
							<Button
								text={'Create Course'}
								styleclass={stylebtn}
								className='form-control'
								onClick={() => saveCourseChange(false)}
							/>
						</div>
						<div className='createcourse-right'>
							<Button
								text={'Cancel Course'}
								styleclass={stylebtnCancel}
								className='form-control'
								onClick={() => cancelCreateCourse(false)}
							/>
						</div>
					</div>
				</div>
				<div className='row'>
					<div className='col'>
						<label>Description:</label>
						<br />
						<textarea
							className='form-control'
							placeholder='Description'
							onChange={handleInputChange}
							name={'description'}
						></textarea>
					</div>
				</div>
			</div>
			<div className='form-group createcourse-author'>
				<div className='row'>
					<div className='col-md-6'>
						<div className='row'>
							<div className='titles'>
								<h6>Add Author</h6>
							</div>
							<div className='agregarnombre'>
								<Input
									id={'name'}
									name={'Author name'}
									placeholder={'Enter author name'}
									style={style}
									onChange={(e) => onChangeAuthorName(e.currentTarget.value)}
									value={authorname}
									type={'text'}
								/>
							</div>
							<br />
							<div className='titles'>
								<Button
									text={'Create Author'}
									styleclass={stylebtn}
									className='form-control'
									onClick={() => addAuthors(authorname)}
								/>
							</div>
						</div>
						<br />
						<div className='row'>
							<div className='titles duration-title'>
								<h6>Duration</h6>
							</div>
							<div>
								<Input
									name={'duration'}
									placeholder={'Enter duration in minutes'}
									onChange={(e) => onChangeDuration(e)}
									type={'number'}
								/>
							</div>
							<br />
						</div>
						<br />
						<div className='row'>
							<div>
								{duration !== '' ? <h4>Duration:{duration} hours</h4> : null}
							</div>
						</div>
					</div>
					<div className='col-md-6'>
						<div className='titles'>
							<h6>Authors</h6>
						</div>
						<ListarAutores key={uuidListaAuthores + 'b'} />
						<div className='courseauhtors'>
							<div className='titles'>
								<h6>Course Authors</h6>
							</div>
						</div>
						<div>
							<ListCourseAuthors key={uuidListaCurso + 'a'} />
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default CreateCourse;
